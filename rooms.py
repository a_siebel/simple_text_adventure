import items
import easygui

class Room:
    def __init__(self, state, text, question):
        self.text : str = text
        self.question : str = question
        self.answer : str = ""
        self.next_room : Room = ""
        self.state = state
        
    def get_answer(self):
        self.answer = input(self.question)
        self.next_room = self
        
class InformatikRaum(Room):
    
    def __init__(self, state):
        super().__init__(state,
                         "Dies ist der Informatikraum. Hier funktioniert die Technik nicht",
                         "Willst du die Technik reparieren?")


    def get_answer(self):
        print("Huch, du findest einen Schlüssel")
        schluessel = item.Item("Schlüssel")
        self.state.append_item("Schlüssel", schluessel)
        self.answer = input(self.question)
        # self.answer = easygui.ynbox("Huch, du findest einen Schlüssel" +
        # self.question, "Mein Textadventure", ('Yes', 'No'))
        print(self.answer)
        if self.answer == "y": # if you use ynbox, replace with `if self.answer == True`
            self.next_room = self.state.get_room("Informatikraum")
        if not self.answer == "y": # if you use ynbox, replace with `if self.answer == False`
            self.next_room = self.state.get_room("Lehrerzimmer")
        
class Lehrerzimmer(Room):
    def __init__(self, state):
        super().__init__(state,
                         "Dies ist das Lehrerzimmer. Es ist abgeschlossen....",
                         "Willst du aufschließen?")
    
    def get_answer(self):
        self.answer = input(self.question)
        print("Get answer in lehrerzimmer")
        if self.answer == "y":
            if "Schlüssel" in self.state.items:
                lehrerzimmer_offen = LehrerzimmerOffen(self.state)
                self.state.change_room("Lehrerzimmer", lehrerzimmer_offen)
                self.next_room = lehrerzimmer_offen
            else:
                self.next_room = self
        else:
            self.next_room = self
                
class LehrerzimmerOffen(Room):
    def __init__(self, state):
        super().__init__(state,
                         "Dies ist das Lehrerzimmer. Es ist offen....",
                         "Das Lehrerzimmer ist offen")
           
    
