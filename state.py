import rooms
import items
class State:
    def __init__(self):
        self.current_room : rooms.Room  = None
        self.rooms : dict = {}
        self.items : dict = {}
        
    def switch_room(self, new_room):
        self.current_room = new_room
        
    def append_room(self, identifier : str, room: rooms.Room):
        self.rooms[identifier] = room

    def change_room(self, identifier : str, room: rooms.Room):
        self.rooms[identifier] = room

    def append_item(self, identifier : str, item: items.Item):
        self.items[identifier] = item
        
    def get_room(self, identifier : str):
        return self.rooms[identifier]