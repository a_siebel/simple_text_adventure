import state as st
import rooms
state = st.State()
informatikraum = rooms.InformatikRaum(state)
lehrerzimmer = rooms.Lehrerzimmer(state)
state.append_room("Lehrerzimmer", lehrerzimmer)
state.append_room("Informatikraum", informatikraum)
state.current_room = informatikraum
       
quit = False

while not quit:
    print(type(state.current_room))
    print(state.current_room.text)
    state.current_room.get_answer()
    state.current_room = state.current_room.next_room
    
