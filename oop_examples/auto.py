class Auto():
    def __init__(self, farbe):
        self.farbe = farbe
        
class LKW(Auto):
    def __init__(self, farbe, anhaenger):
        super().__init__(farbe)
        self.anhaenger = anhaenger
        
mein_auto = Auto("rot")
print(mein_auto.farbe)
print(type(mein_auto))

mein_lkw = LKW("blau", 3)
print(mein_lkw, mein_lkw.farbe, mein_lkw.anhaenger)
        

print(isinstance(mein_auto, Auto))
print(isinstance(mein_lkw, Auto))
print(isinstance(mein_auto, LKW))